import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Student> result = new ArrayList<>();

        String SQL_SELECT = "Select * from Student";

        try (
                Connection conn = DriverManager.getConnection(
                        "jdbc:postgresql://127.0.0.1:5432/Cs01", "postgres", "123");
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("studentId");
                String name = resultSet.getString("StudentName");
                long phone = resultSet.getLong("phone");
                int groupId = resultSet.getInt("groupId");

                Student obj = new Student();
                obj.setStudentId(id);
                obj.setStudentName(name);
                obj.setPhone(phone);

                obj.setGroupId(groupId);

                result.add(obj);

            }
            result.forEach(x -> System.out.println(x));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
